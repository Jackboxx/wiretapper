local session = {}

---@type uv.uv_tcp_t|uv_tcp_t|nil
local server = nil

function session.start(opts)
    session.close()

    server = vim.loop.new_tcp()
    assert(server, "failed to start tcp server")

    local port = opts.session.port
    local _success, err = server:bind("0.0.0.0", port)
    assert(not err, err)

    local _success, err = server:listen(opts.session.max_clients, function()
        local client = vim.loop.new_tcp()
        assert(client, "failed to start connection with client")
        server:accept(client)

        local authenticated = not opts.auth.password
        client:read_start(function(err, data)
            assert(not err, err)

            if data ~= nil and server ~= nil and not server:is_closing() then
                if not authenticated then
                    if data == opts.auth.password then
                        authenticated = true
                    else
                        client:write("missing/incorrect password")
                        client:shutdown()
                        client:close()
                    end
                else
                    local keys = data:gsub("\n$", "")
                    vim.api.nvim_input(keys)
                end
            else
                client:shutdown()
                client:close()
            end
        end)
    end)
    assert(not err, err)
end

function session.close()
    if server ~= nil and not server:is_closing() then
        server:shutdown()
        server:close()
    end
end

return session
