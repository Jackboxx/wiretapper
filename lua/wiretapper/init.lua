local session = require('wiretapper.__session')
local defaults = require('wiretapper.__defaults')

local wiretapper = {
    config = defaults
}

function wiretapper.setup(opts)
    wiretapper.config = vim.tbl_deep_extend('force', wiretapper.config, opts or {})
end

function wiretapper.start(opts)
    session.start(vim.tbl_deep_extend('force', wiretapper.config, opts or {}))
end

function wiretapper.close()
    session.close()
end

return wiretapper
