return {
    session = {
        port = 8181,
        max_clients = 256,
    },
    auth = {
        password = nil,
    }
}
