# wiretapper.nvim

`wiretapper.nvim` is a small plugin that lets you send keys to a remote neovim instance over
TCP. 
The intended purpose is near zero latency pair programming sessions with people in your
local network but if you find a different use case more power to you.

### what does it do

- Low latency keyboard input transfer over a network
- Basic authentication through passwords

### what doesn't it do

- Share buffers or file contents to clients, only keyboard input is sent over the wire
- Keep logs of who sent what when, input tractability is fundamentally not a concern of
  this project

### plugin installation 

#### packer

```lua
    use 'https://gitlab.com/Jackboxx/wiretapper'
```

#### lazy

```lua
{
    'https://gitlab.com/Jackboxx/wiretapper'
}
```

### client installation

```sh
cargo install nvim-wiretapper-client # or grab a Linux binary from 'https://gitlab.com/Jackboxx/wiretapper/-/releases'
```

### how to use it

#### plugin

Start sharing your current neovim session as a server by running

```vim
lua require('wiretapper').start()
```

When you're done you can close the server by running

```vim
lua require('wiretapper').close()
```

#### client

To connect to a server running on the default port (`8181`) on the IP address
`192.168.0.25` run the following and start typing as if you were in a neovim buffer

```sh
nvim-wiretapper-client -H 192.168.0.25
```

If the server has a password run the following

```sh
nvim-wiretapper-client -H 192.168.0.25 -P "my-amazing-password"
```

### how to configure it

```lua
-- all available config options
require('wiretapper').setup({
    session = {
        port = 8181,
        max_clients = 256,
    },
    auth = {
        password = nil, -- set to a string (e.g "my-amazing-password") to enable password authentication
    }
})
```
