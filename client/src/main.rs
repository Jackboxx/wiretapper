#![deny(clippy::implicit_return)]
#![allow(clippy::needless_return)]

use std::io;

use clap::Parser;
use std::io::{stdin, stdout, Write};
use std::net::TcpStream;
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct CliArgs {
    #[arg(short = 'H', long)]
    /// IP address of the nvim instance acting as the server, e.g. 192.168.0.50.
    host: String,
    #[arg(short, long, default_value_t = String::from("8181"))]
    /// Port of the nvim instance acting as the server.
    port: String,
    #[arg(short = 'P', long)]
    /// Password required by the server for initial connection.
    password: Option<String>,
}

fn main() -> io::Result<()> {
    let CliArgs {
        host,
        port,
        password,
    } = CliArgs::parse();
    let mut stream = TcpStream::connect(format!("{host}:{port}"))?;

    if let Some(password) = password {
        stream.write_all(password.as_bytes())?;
    };

    return listen_for_keys(stream);
}

fn listen_for_keys(mut stream: TcpStream) -> io::Result<()> {
    let stdin = stdin();
    let mut stdout = stdout().into_raw_mode()?;
    stdout.flush()?;

    for c in stdin.keys() {
        let vim_keys = keycode_to_vim_char(&c?);
        stream.write_all(vim_keys.as_bytes())?;

        stdout.flush()?;
    }

    return Ok(());
}

fn keycode_to_vim_char(code: &Key) -> String {
    return match code {
        Key::Char(c) => char_as_vim_char(c),
        Key::Ctrl(c) => format!("<C-{}>", char_as_vim_char(c)),
        Key::Alt(c) => format!("<A-{}>", char_as_vim_char(c)),
        Key::F(n) => format!("<F{n}>"),
        Key::Esc => "<Esc>".to_owned(),
        Key::Backspace => "<Bs>".to_owned(),
        Key::Up => "<Up>".to_owned(),
        Key::Down => "<Down>".to_owned(),
        Key::Left => "<Left>".to_owned(),
        Key::Right => "<Right>".to_owned(),
        Key::Home => "<Home>".to_owned(),
        Key::End => "<End>".to_owned(),
        Key::Insert => "<Insert>".to_owned(),
        Key::Null => "<Nul>".to_owned(),
        Key::Delete => "<Del>".to_owned(),
        Key::PageUp => "<PageUp>".to_owned(),
        Key::PageDown => "<PageDown>".to_owned(),
        Key::BackTab => "".to_owned(),
        Key::__IsNotComplete => "".to_owned(),
    };
}

fn char_as_vim_char(c: &char) -> String {
    return match c {
        '\n' => "<Cr>".to_owned(),
        '\t' => "<Tab>".to_owned(),
        ' ' => "<Space>".to_owned(),
        _ => c.to_string(),
    };
}
